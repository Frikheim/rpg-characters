Readme RPG-characters

Two abstract classes, item and character.

4 subclasses which extends character, Rogue, Ranger, Warrior and Mage.

Characters have base and total attributes, can level up, attack and equip weapons and armor.

2 subclasses which extends item, Weapon and Armor.

Items have a slot, level requirement and type and can only be equipped by valid characters.

Weapons have damage and attacks per second.

Armor have attributes.

Tests of the functionality is included