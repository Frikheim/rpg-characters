package com.company.items;

import com.company.characters.PrimaryAttributes;
import com.company.characters.Warrior;
import com.company.errors.InvalidArmorException;
import com.company.errors.InvalidWeaponException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ItemTest {

    @Test
    public void equipWeapon_axeLvl2_throwExceptionLowLvl () {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        String expected = "Too low level to equip this weapon";
        Weapon axe = new Weapon("Common Axe",2,SlotTypes.WEAPON,WeaponTypes.AXE,7,1.1);
        //act
        String actual = assertThrows(InvalidWeaponException.class, ()->warrior.equipWeapon(axe)).getMessage();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equipArmor_plateLvl2_throwExceptionLowLvl () {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        String expected = "Too low level to equip this armor";
        Armor plate = new Armor("Common Plate Body Armor",2,SlotTypes.BODY,ArmorTypes.PLATE,new PrimaryAttributes(2,1,0,0));
        //act
        String actual = assertThrows(InvalidArmorException.class, ()->warrior.equipArmor(plate)).getMessage();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equipWeapon_bow_throwExceptionWrongType () {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        String expected = "Warriors can only equip sword, hammer or axe";
        Weapon bow = new Weapon("Common Bow",1,SlotTypes.WEAPON,WeaponTypes.BOW,12,0.8);
        //act
        String actual = assertThrows(InvalidWeaponException.class, ()->warrior.equipWeapon(bow)).getMessage();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equipArmor_cloth_throwExceptionWrongType () {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        String expected = "Warriors can only equip mail or plate armor";
        Armor plate = new Armor("Common Cloth Head Armor",1,SlotTypes.HEAD,ArmorTypes.CLOTH,new PrimaryAttributes(1,0,0,5));
        //act
        String actual = assertThrows(InvalidArmorException.class, ()->warrior.equipArmor(plate)).getMessage();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equipWeapon_axe_returnsTrue () throws InvalidWeaponException {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        boolean expected = true;
        Weapon axe = new Weapon("Common Axe",1,SlotTypes.WEAPON,WeaponTypes.AXE,7,1.1);
        //act
        boolean actual = warrior.equipWeapon(axe);
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void equipArmor_plate_returnsTrue () throws InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        boolean expected = true;
        Armor plate = new Armor("Common Plate Body Armor",1,SlotTypes.BODY,ArmorTypes.PLATE,new PrimaryAttributes(2,1,0,0));
        //act
        boolean actual = warrior.equipArmor(plate);
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void DPS_noWeapon_returns1Point05 () {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        double expected = 1.0*(1.0 + (5.0 / 100.0));
        //act
        double actual = warrior.attack();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void DPS_axe_returns8Point085 () throws InvalidWeaponException {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        double expected = (7.0*1.1)*(1.0 + (5.0 / 100.0));
        Weapon axe = new Weapon("Common Axe",1,SlotTypes.WEAPON,WeaponTypes.AXE,7,1.1);
        //act
        warrior.equipWeapon(axe);
        double actual = warrior.attack();
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    public void DPS_axePlate_returns8Point162 () throws InvalidWeaponException, InvalidArmorException {
        //Arrange
        Warrior warrior = new Warrior("warrior");
        double expected = (7.0*1.1)*(1.0 + ((5.0+1.0) / 100.0));
        Weapon axe = new Weapon("Common Axe",1,SlotTypes.WEAPON,WeaponTypes.AXE,7,1.1);
        Armor plate = new Armor("Common Plate Body Armor",1,SlotTypes.BODY,ArmorTypes.PLATE,new PrimaryAttributes(2,1,0,0));
        //act
        warrior.equipWeapon(axe);
        warrior.equipArmor(plate);
        double actual = warrior.attack();
        //Assert
        assertEquals(expected,actual);
    }



}