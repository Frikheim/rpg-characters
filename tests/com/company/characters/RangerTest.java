package com.company.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RangerTest {

    @Test
    void createRanger_name_validAttributes() {
        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(8,1,7,1);
        //Act
        Ranger ranger = new Ranger("ranger");
        PrimaryAttributes actual = ranger.baseAttributes;
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpRanger_name_validAttributes() {

        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(10, 2, 12, 2);
        Ranger ranger = new Ranger("ranger");

        //Act
        ranger.levelUp();
        PrimaryAttributes actual = ranger.baseAttributes;

        //Assert
        assertEquals(expected, actual);
    }
}