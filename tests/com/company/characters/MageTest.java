package com.company.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MageTest {

    @Test
    void createMage_name_validAttributes() {
        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(5,1,1,8);
        //Act
        Mage mage = new Mage("mage");
        PrimaryAttributes actual = mage.baseAttributes;
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpMage_name_validAttributes() {

        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(8, 2, 2, 13);
        Mage mage = new Mage("mage");

        //Act
        mage.levelUp();
        PrimaryAttributes actual = mage.baseAttributes;

        //Assert
        assertEquals(expected, actual);
    }
}