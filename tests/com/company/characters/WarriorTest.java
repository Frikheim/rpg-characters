package com.company.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarriorTest {

    @Test
    void createWarrior_name_validAttributes() {
        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(10,5,2,1);
        //Act
        Warrior warrior = new Warrior("warrior");
        PrimaryAttributes actual = warrior.baseAttributes;
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void levelUpWarrior_name_validAttributes() {

        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(15, 8, 4, 2);
        Warrior warrior = new Warrior("warrior");
        //Act
        warrior.levelUp();
        PrimaryAttributes actual = warrior.baseAttributes;
        //Assert
        assertEquals(expected, actual);
    }
}