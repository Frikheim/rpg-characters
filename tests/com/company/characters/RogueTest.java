package com.company.characters;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RogueTest {

    @Test
    void levelUpRogue_name_validAttributes() {
        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(11,3,10,2);
        Rogue rouge = new Rogue("rogue");
        //Act
        rouge.levelUp();
        PrimaryAttributes actual = rouge.baseAttributes;
        //Assert
        assertEquals(expected,actual);
    }

    @Test
    void createRogue_name_validAttributes() {

        //Arrange
        PrimaryAttributes expected = new PrimaryAttributes(8,2,6,1);

        //Act
        Rogue rouge = new Rogue("rogue");
        PrimaryAttributes actual = rouge.baseAttributes;
        //Assert
        assertEquals(expected,actual);
    }
}