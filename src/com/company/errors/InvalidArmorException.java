package com.company.errors;

public class InvalidArmorException extends Exception {
    public InvalidArmorException(String message) {
        super(message);
    }
}
