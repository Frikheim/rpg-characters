package com.company.items;

public abstract class Item {
    protected String name;
    protected int levelReq;
    protected SlotTypes slot;

    public int getLevelReq() {
        return levelReq;
    }

    public SlotTypes getSlot() {
        return slot;
    }


    public Item(String name, int levelReq, SlotTypes slot) {
        this.name = name;
        this.levelReq = levelReq;
        this.slot = slot;
    }
}
