package com.company.items;

import com.company.characters.PrimaryAttributes;

public class Armor extends Item{

    private final ArmorTypes type;
    private final PrimaryAttributes attributes;

    public Armor(String name, int levelReq, SlotTypes slot, ArmorTypes type, PrimaryAttributes attributes) {
        super(name, levelReq, slot);
        this.type = type;
        this.attributes = attributes;
    }

    public PrimaryAttributes getAttributes() {
        return attributes;
    }

    public ArmorTypes getType() {
        return type;
    }
}
