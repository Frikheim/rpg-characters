package com.company.items;

public class Weapon extends Item {
    private int damage;
    private double attacksPerSecond;
    private WeaponTypes type;

    public Weapon(String name, int levelReq, SlotTypes slot, WeaponTypes weaponType, int damage, double attacksPerSecond) {
        super(name,levelReq,slot);
        this.damage = damage;
        this.attacksPerSecond = attacksPerSecond;
        this.type = weaponType;
    }

    /**
     * Method that calculates DPS from damage and attacks per second
     * @return the weapon DPS
     */
    public double getDPS() {
        return damage * attacksPerSecond;
    }

    public WeaponTypes getType() {
        return type;
    }
}
