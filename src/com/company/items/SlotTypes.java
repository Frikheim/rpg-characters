package com.company.items;

public enum SlotTypes {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
