package com.company.items;

public enum ArmorTypes {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
