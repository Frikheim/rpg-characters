package com.company.characters;

import com.company.errors.InvalidArmorException;
import com.company.errors.InvalidWeaponException;
import com.company.items.*;

public class Warrior extends Character {

    public Warrior(String name) {
        super(name, new PrimaryAttributes(10,5,2,1)
                , new PrimaryAttributes(10,5,2,1));
    }

    /**
     * Method for leveling up a character
     * increases base attributes and level
     * calls the updateTotalAttributes method
     */
    @Override
    public void levelUp() {
        //update base attributes for lvl up
        baseAttributes.increaseAttributes(new PrimaryAttributes(5,3,2,1));
        //update total attributes for lvl up
        updateTotalAttributes();
        level++;

    }

    /**
     * Method for equiping weapon
     * @param weapon weapon that is equipped
     * @return true if the weapon is equipped
     * @throws InvalidWeaponException if the lvlRequirement is too high or the weapon is the wrong type
     */
    public boolean equipWeapon(Weapon weapon) throws InvalidWeaponException {
        if(level < weapon.getLevelReq()) {
            throw new InvalidWeaponException("Too low level to equip this weapon");
        }
        else if(weapon.getType() == WeaponTypes.SWORD || weapon.getType() == WeaponTypes.HAMMER || weapon.getType() == WeaponTypes.AXE) {
            items.put(SlotTypes.WEAPON,weapon);
            return true;
        }
        else {
            throw new InvalidWeaponException("Warriors can only equip sword, hammer or axe");
        }
    }

    /**
     * method for equipping armor
     * @param armor that is to be equipped
     * @return true if equipped successfully
     * @throws InvalidArmorException if the lvlRequirement is too high or the armor is the wrong type
     */
    public boolean equipArmor(Armor armor) throws InvalidArmorException {
        if(level < armor.getLevelReq()) {
            throw new InvalidArmorException("Too low level to equip this armor");
        }
        else if(armor.getType() == ArmorTypes.MAIL || armor.getType() == ArmorTypes.PLATE ) {
            items.put(armor.getSlot(),armor);
            updateTotalAttributes();
            return true;
        }
        else {
            throw new InvalidArmorException("Warriors can only equip mail or plate armor");
        }
    }

    /**
     * Method for attacking
     * uses the equpped weapons damage or a default value of 1
     * @return the characters dps
     */
    @Override
    public double attack() {
        Weapon weapon =(Weapon) items.get(SlotTypes.WEAPON);
        if (!(weapon == null)) {
            return weapon.getDPS() * (1.0+(totalAttributes.getStrength()/100.0));
        }
        else {
            return 1.0+(totalAttributes.getStrength()/100.0);
        }
    }

    @Override
    public String toString() {
        return "Warrior{" +
                "name='" + name + '\'' +
                ", level=" + level +
                ", baseAttributes" + baseAttributes.toString() +
                ", totalAttributes" + totalAttributes.toString() +
                ", DPS=" + attack() +
                '}';
    }
}
