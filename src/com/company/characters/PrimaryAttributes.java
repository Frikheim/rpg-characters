package com.company.characters;

import java.util.Objects;

public class PrimaryAttributes {
    private int strength;
    private int dexterity;
    private int intelligence;
    private int vitality;

    @Override
    public String toString() {
        return "{" +
                "strength=" + strength +
                ", dexterity=" + dexterity +
                ", intelligence=" + intelligence +
                ", vitality=" + vitality +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PrimaryAttributes)) return false;
        PrimaryAttributes that = (PrimaryAttributes) o;
        return strength == that.strength && dexterity == that.dexterity && intelligence == that.intelligence && vitality == that.vitality;
    }

    @Override
    public int hashCode() {
        return Objects.hash(strength, dexterity, intelligence, vitality);
    }

    public PrimaryAttributes(int vitality, int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.vitality = vitality;
    }
    // increase attributes
    public void increaseAttributes(PrimaryAttributes attributes) {
        this.vitality += attributes.getVitality();
        this.strength += attributes.getStrength();
        this.dexterity += attributes.getDexterity();
        this.intelligence += attributes.getIntelligence();
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }
    public void increaseStrength(int strength) {
        this.strength += strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }
    public void increaseDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }
    public void increaseIntelligence(int intelligence) {
        this.intelligence += intelligence;
    }

    public int getVitality() {
        return vitality;
    }

    public void setVitality(int vitality) {
        this.vitality = vitality;
    }
    public void increaseVitality(int vitality) {
        this.vitality += vitality;
    }
}
