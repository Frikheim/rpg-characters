package com.company.characters;

import com.company.errors.InvalidArmorException;
import com.company.errors.InvalidWeaponException;
import com.company.items.Armor;
import com.company.items.Item;
import com.company.items.SlotTypes;
import com.company.items.Weapon;

import java.util.Collection;
import java.util.HashMap;

public abstract class Character {
    protected String name;
    protected int level;
    protected PrimaryAttributes baseAttributes;
    protected PrimaryAttributes totalAttributes;
    protected HashMap<SlotTypes, Item> items;

    public Character(String name, PrimaryAttributes baseAttributes, PrimaryAttributes totalAttributes) {
        this.name = name;
        items = new HashMap<>();
        level = 1;
        this.baseAttributes = baseAttributes;
        this.totalAttributes = totalAttributes;
    }

    public abstract void levelUp();
    public abstract double attack();

    public abstract boolean equipWeapon(Weapon weapon) throws InvalidWeaponException, InvalidArmorException;
    public abstract boolean equipArmor(Armor armor) throws InvalidWeaponException, InvalidArmorException;


    /**
     * Method that updates the totalattributes
     * sets the value equal to base attributes, then loops through items and adds the bonus attributes from armor
     */
    public void updateTotalAttributes() {
        totalAttributes = new PrimaryAttributes(baseAttributes.getVitality(), baseAttributes.getStrength(), baseAttributes.getDexterity(), baseAttributes.getIntelligence());
        for (Item item: items.values() ) {
            if (item instanceof Armor) {
                totalAttributes.increaseAttributes(((Armor) item).getAttributes());
            }
        }
    }

}
